<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $itemsPerPage = 10;
        $posts = Post::orderBy('created_at', 'desc')->paginate($itemsPerPage);
        return view('post.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create', array('title' => 'Add News'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $id = Auth::id();
        DB::table('posts')->insert(
            ['title' => $request->nom, 'subject' => $request->sujet, 'slug'=> str_slug($request->nom), 'content' => $request->content, 'author_id' => $id ]
        );
        return redirect(route('posts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param \App\Page $page
     */
    public function show($id)
    {
        // get the nerd
        $lapage = Post::findOrFail($id);

        // show the view and pass the nerd to it
        return view('post.show', compact('lapage') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('post.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id )
    {
        $post = Post::findOrFail($id);
        $input = $request->all();
        DB::table('posts')
            ->where('id', $id)
            ->update(['title' => $request->nom, 'subject' => $request->sujet, 'content' => $request->content]);
        return redirect(route('post.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $page = Post::findOrFail($id);
        $page->delete();

        // redirect
        session()->flash('message', 'Nous avons bien supprimé le post');
        return redirect(route('posts.index'));
    }
}
