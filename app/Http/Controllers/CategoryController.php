<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $itemsPerPage = 10;
        $categorys = Category::orderBy('created_at', 'desc')->paginate($itemsPerPage);
        return view('category.index')->with('categorys', $categorys);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create', array('title' => 'Add News'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $id = Auth::id();
        DB::table('categories')->insert(
            ['title' => $request->nom, 'slug'=> str_slug($request->nom), 'author_id' => $id ]
        );
        return redirect(route('categories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param \App\Page $page
     */
    public function show($id)
    {
        // get the nerd
        $lapage = Category::findOrFail($id);

        // show the view and pass the nerd to it
        return view('category.show', compact('lapage') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id )
    {
        $category = Category::findOrFail($id);
        $input = $request->all();
        DB::table('categories')
            ->where('id', $id)
            ->update(['title' => $request->nom, 'subject' => $request->sujet,  'content' => $request->content]);
        return redirect(route('categories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $page = Category::findOrFail($id);
        $page->delete();

        // redirect
        session()->flash('message', 'Nous avons bien supprimé la catégorie');
        return redirect(route('categories.index'));
    }
}
