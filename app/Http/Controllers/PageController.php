<?php

namespace App\Http\Controllers;

use App\Page;
use App\Post;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $itemsPerPage = 10;
        $pages = Page::orderBy('created_at', 'desc')->paginate($itemsPerPage);
        return view('page.index')->with('pages', $pages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page.create', array('title' => 'Add News'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $id = Auth::id();
        DB::table('pages')->insert(
            ['title' => $request->nom, 'subject' => $request->sujet,  'slug'=> str_slug($request->nom), 'content' => $request->content, 'author_id' => $id,  "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now(), ]
        );
        return redirect(route('pages.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        // get the nerd
        $lapage = Page::findOrFail($id);

        // show the view and pass the nerd to it
        return view('page.show', compact('lapage') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::findOrFail($id);
        return view('page.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id )
    {
        $page = Page::findOrFail($id);
        $input = $request->all();
        DB::table('pages')
            ->where('id', $id)
            ->update(['title' => $request->nom, 'subject' => $request->sujet, 'content' => $request->content]);
        return redirect(route('pages.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $page = Page::findOrFail($id);
        $page->delete();

        // redirect
        session()->flash('message', 'Successfully deleted the page!');
        return redirect(route('pages.index'));
    }



    public function pagetype($content)
    {
        $type = $content;
        $itemsPerPage = 10;
        $pages = Page::orderBy('created_at', 'desc')->paginate($itemsPerPage)->where('subject', '=', $type);
        $posts = Post::orderBy('created_at', 'desc')->paginate($itemsPerPage)->where('subject', '=', $type);
        return view('page.indextype')->with('pages', $pages)->with('type', $type)->with('posts', $posts);
    }

}
