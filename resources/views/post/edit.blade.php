@extends('layouts.app')

@section('content')


    <form action="{{route('pages.update', $page->id)}}" method="POST" id="demo-form">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PATCH">
        <div class="field">
            <label class="label">Nom de la page</label>
            <p class="control">
                <input class="input" type="text" placeholder="Text input" name="nom"  value="{{ $page->title }}{{ old('nom') }}">
            </p>
        </div>



        <div class="field">
            <label class="label">Sujet</label>
            <p class="control">
                <input class="input" id="q" type="text" placeholder="Chercher" name="sujet" value="{{ $page->subject }}{{ old('sujet') }}">
            </p>
        </div>

        <div class="field">
            <label class="label">Contenu</label>
            <p class="control">
                <textarea id="my-editor" name="content" class="form-control" value="{{ $page->content }}{{ old('content') }}">{!!$page->content !!}</textarea>
            </p>
        </div>



        <div class="field is-grouped">
            <p class="control">
                <button class="button is-primary g-recaptcha" type="submit" data-sitekey="6LeJvycUAAAAADO6jyeJ8jO-aHRqlcdbIPJSoM6u" data-callback='onSubmit'>Envoyer</button>
            </p>
            <p class="control">
                <button class="button is-link" type="reset">Annuler</button>
            </p>
        </div>

    </form>

@endsection
