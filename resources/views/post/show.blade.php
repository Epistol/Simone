@extends('layouts.app')

@section('content')

    <nav class="breadcrumb">
        <ul>
            <li><a>  {{ config('app.name', 'Laravel') }}</a></li>
            <li><a>{{ $lapage->subject }}</a></li>
        </ul>
    </nav>


    <h1 class="title is-1">{{ $lapage->title }}</h1>

<div class="jumbotron text-center">
    <p>
       {!! $lapage->content !!}<br>
    </p>
</div>


@endsection
