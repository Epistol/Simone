<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Styles -->

    <link href="http://bulma.io/css/bulma-docs.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui.min.css') }}" rel="stylesheet">
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-3386226072112083",
            enable_page_level_ads: true
        });
    </script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
        function onSubmit(token) {
            document.getElementById("demo-form").submit();
        }
    </script>
</head>
<body>
    <div id="app">
        <header class="sm-header">
{{--
        <img src="http://via.placeholder.com/150x150?text=Merci+Simone" />--}}

{{--        <nav class="nav has-shadow">
            <div class="container">
                <div class="nav-left">
                    <a class="nav-item"  href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>

                </div>
                <span class="nav-toggle">
      <span></span>
      <span></span>
      <span></span>
    </span>
                <div class="nav-right nav-menu">

                    @if (Auth::guest())
                        <a class="nav-item is-tab " href="{{ route('login') }}">Connexion</a>
                        <a  class="nav-item is-tab" href="{{ route('register') }}">Inscription</a>
                    @else
                    <a class="nav-item is-tab">
                        <figure class="image is-16x16" style="margin-right: 8px;">
                            <img src="http://bulma.io/images/jgthms.png">
                        </figure>
                        Profile
                    </a>
                    <a class="nav-item is-tab">Log out</a>
                    @endif
                </div>
            </div>
        </nav>--}}


        <nav class="nav nav-simone has-shadow">

            <span id="nav-toggle" class="nav-toggle ">
    <span></span>
    <span></span>
    <span></span>
  </span>

            <div class="nav-left">
                <a class="nav-item"  href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>

            <div class="nav-right nav-menu" id="nav-menu">

                <a class="nav-simone-item" href="/IVG">IVG</a>
                <a class="nav-simone-item" >SEXUALITE</a>
                <a class="nav-simone-item" >CONTRACEPTION</a>
                <a class="nav-simone-item" >VIOLENCES</a>
                <a class="nav-simone-item" >HARCELEMENT</a>
                <a class="nav-simone-item" >DROIT DES FEMMES</a>
                <a class="nav-simone-item" >SIMONE</a>



            </div>



        </nav>

        </header>

        <div class="container">
        @yield('content')
        </div>
    </div>

    @include('layouts.footer')

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/delete.js') }}"></script>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="http://bulma.io/javascript/bulma.js"></script>

    <script>
        var options = {

            filebrowserBrowseUrl: '/elfinder/ckeditor'

        };
        CKEDITOR.replace( 'my-editor', options );

    </script>


</body>
</html>
