@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!

                    <a class="button is-primary" href="{{route('pages.index')}}">Nouvelle page</a>
                    <a class="button is-primary" href="{{route('posts.index')}}">Nouveau post</a>
                    <a class="button is-primary" href="{{route('categories.index')}}">Nouvelle categorie</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
