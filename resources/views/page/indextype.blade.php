@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="title is-1 tag tagpage">{{$type}}</h1>
        <!-- will be used to show any messages -->
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        @foreach($pages as $key => $value)
        <div class="content">
            {!! $value->content !!}
        </div>
        @endforeach

<?php $clef = 0; ?>

            @foreach($posts as $key => $value)


            <?php $string = str_slug($value->title);?>



                @if($clef % 3 == 0)
                <div class="tile is-ancestor">
                    @endif

                <div class="tile is-parent">
                    <article class="tile is-child box">
                        <p class="title"><a href="{{$value->subject}}/{{$string}}">{{$value->title}}</a> </p>
                        <p >{!! \Illuminate\Support\Str::words($value->content, 100) !!}</p>
                    </article>
                </div>

                    @if($clef % 3 == 2)
                </div>
                            @endif
            <?php $clef++; ?>
            @endforeach

    </div>
@endsection
