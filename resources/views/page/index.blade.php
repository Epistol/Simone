@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Les pages</h1>

        <a class="button is-primary" href="{{ route('pages.create') }}">Nouvelle</a>

        <!-- will be used to show any messages -->
        @if (Session::has('message'))
            <div class="notification">{{ Session::get('message') }}</div>
        @endif

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <td>ID</td>
                <td>Titre</td>
                <td>Sujet</td>
                <td>Actions</td>
            </tr>
            </thead>
            <tbody>
            @foreach($pages as $key => $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->title }}</td>
                    <td>{{ $value->subject }}</td>

                    <!-- we will also add show, edit, and delete buttons -->
                    <td>

                        <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                        <!-- we will add this later since its a little more complicated than the other two buttons -->

                        <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                        <a class="button" href="{{ URL::to('pages/' . $value->id) }}">Afficher</a>

                        <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                        <a class="button" href="{{ URL::to('pages/' . $value->id . '/edit') }}">Editer</a>
                        <a data-method="delete"
                           data-token="{{csrf_token()}}" data-confirm="Are you sure?"
                           class="button" href="{{ URL::to('pages/' . $value->id ) }}">Supprimer</a>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
