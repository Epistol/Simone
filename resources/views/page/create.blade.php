@extends('layouts.app')

@section('content')


    <form action="{{route('pages.store')}}" method="POST" id="demo-form">
        {{ csrf_field() }}
    <div class="field">
        <label class="label">Nom de la page</label>
        <p class="control">
            <input class="input" type="text" placeholder="Nom de la page" name="nom">
        </p>
    </div>



    <div class="field">
        <label class="label">Sujet</label>
        <p class="control">
            <input class="input" id="q" type="text" placeholder="Chercher" name="sujet">
        </p>
    </div>

    <div class="field">
        <label class="label">Contenu</label>
        <p class="control">
            <textarea id="my-editor" name="content" class="form-control">{!! old('content', 'test editor content') !!}</textarea>
        </p>
    </div>



    <div class="field is-grouped">
        <p class="control">
                <button class="button is-primary g-recaptcha" type="submit" data-sitekey="6LeJvycUAAAAADO6jyeJ8jO-aHRqlcdbIPJSoM6u" data-callback='onSubmit'>Envoyer</button>
        </p>
        <p class="control">
            <button class="button is-link" type="reset">Annuler</button>
        </p>
    </div>

    </form>

@endsection
