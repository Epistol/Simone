@extends('layouts.app')

@section('content')


    <form action="{{route('categories.store')}}" method="POST" id="demo-form">
        {{ csrf_field() }}
    <div class="field">
        <label class="label">Nom de la catégorie</label>
        <p class="control">
            <input class="input" type="text" placeholder="Nom de la catégorie" name="nom">
        </p>
    </div>



    <div class="field is-grouped">
        <p class="control">
                <button class="button is-primary g-recaptcha" type="submit" data-sitekey="6LeJvycUAAAAADO6jyeJ8jO-aHRqlcdbIPJSoM6u" data-callback='onSubmit'>Envoyer</button>
        </p>
        <p class="control">
            <button class="button is-link" type="reset">Annuler</button>
        </p>
    </div>

    </form>

@endsection
